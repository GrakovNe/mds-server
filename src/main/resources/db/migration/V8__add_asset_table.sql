create table if not exists asset
(
  id                bigserial not null
    constraint asset_pkey
    primary key,
  created_date_time bytea,
  file_name         varchar(255),
  hash              varchar(255),
  name              varchar(255)
);

