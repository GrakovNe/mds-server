CREATE INDEX author_name ON author (name);
CREATE INDEX rating_value ON rating (value);
CREATE INDEX story_title ON story (title);
CREATE INDEX tag_value ON tag (value);
CREATE INDEX users_username ON users (username);