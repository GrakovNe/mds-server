alter table asset alter column id type integer;
alter table user_rating alter column id type integer;
alter table cover alter column asset_id type integer;
alter table story alter column asset_id type integer;