create table if not exists user_rating
(
  id       bigserial not null,
  value    double precision,
  story_id integer,
  created_date_time bytea,
  user_id  integer
);

