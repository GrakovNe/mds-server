create table if not exists application_metrics
(
  id               serial not null
    constraint application_metrics_pkey
    primary key,
  authors          bigint,
  bookmarks        bigint,
  comments         bigint,
  create_date_time bytea,
  listened_stories bigint,
  listened_time    bigint,
  rated_stories    bigint,
  recent_stories   bigint,
  stories          bigint,
  user_ratings     bigint,
  users            bigint
);

