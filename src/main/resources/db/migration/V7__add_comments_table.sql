create table if not exists comment
(
  id               serial         not null
    constraint comment_pkey
    primary key,
  story_id         integer
    constraint story_comment_fkey
    references story
    on update cascade on delete cascade,
  user_id          integer
    constraint user_comment_fkey
    references users
    on update cascade on delete cascade,
  message          varchar(65535) not null,
  create_date_time bytea,
  user_name VARCHAR(255)
);