CREATE INDEX if not exists story_bookmark_story_user_idx ON story_bookmark (story_id, user_id);
CREATE INDEX if not exists story_bookmark_story_user_created_date_time_idx ON story_bookmark (create_date_time, story_id, user_id);
CREATE INDEX if not exists user_rating_story_user_idx ON user_rating (story_id, user_id);
CREATE INDEX if not exists asset_filename_idx ON asset (file_name);
CREATE INDEX if not exists asset_hash_idx ON asset (hash);