CREATE TABLE story_recent
(
  id serial NOT NULL PRIMARY KEY,
  story_id integer,
  user_id integer,
  create_date_time bytea,
  CONSTRAINT recent_story_user FOREIGN KEY (user_id)
      REFERENCES public.users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT recent_story_story FOREIGN KEY (story_id)
      REFERENCES public.story (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
);