package org.grakovne.mds.server.scheduling;

import org.grakovne.mds.server.providers.ConfigurationProvider;
import org.grakovne.mds.server.services.ApplicationMetricService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class ApplicationMetricJob implements MdsJob {

    private final ConfigurationProvider configurationProvider;
    private final ApplicationMetricService service;

    public ApplicationMetricJob(ConfigurationProvider configurationProvider,
                                ApplicationMetricService service) {

        this.configurationProvider = configurationProvider;
        this.service = service;
    }

    @Scheduled(fixedRate = 30_000 * 30)
    public void execute() {
        if (configurationProvider.getMetrics()) {
            service.createOne();
        }
    }
}
