package org.grakovne.mds.server.scheduling;

public interface MdsJob {

    void execute();
}
