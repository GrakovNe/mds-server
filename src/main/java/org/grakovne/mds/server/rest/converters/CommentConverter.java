package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.Comment;
import org.grakovne.mds.server.rest.responses.CommentResponse;
import org.springframework.stereotype.Service;

@Service
public class CommentConverter implements MdsConverter<Comment, CommentResponse> {

    @Override
    public CommentResponse getBlankResponse() {
        return new CommentResponse();
    }

    @Override
    public CommentResponse feedWith(Comment comment, CommentResponse response) {
        response.setId(comment.getId());
        response.setStoryId(comment.getStory().getId());
        response.setUserId(comment.getUser().getId());
        response.setUsername(comment.getUsername());
        response.setMessage(comment.getMessage());
        response.setCreatedDateTime(comment.getCreatedDateTime());

        return response;
    }
}
