package org.grakovne.mds.server.rest.forms;

public class BookmarkForm implements MdsForm {
    private Integer timestamp;
    private Integer storyId;

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }
}
