package org.grakovne.mds.server.rest.validation;

import org.grakovne.mds.server.exceptons.EntityValidationException;
import org.grakovne.mds.server.rest.forms.UserRatingForm;
import org.springframework.stereotype.Service;

@Service
public class UserRatingValidation implements ValidationService<UserRatingForm> {
    @Override
    public UserRatingForm validate(UserRatingForm form) {
        if (form == null) {
            throw new EntityValidationException(UserRatingForm.class, "should be presented");
        }

        if (form.getStoryId() == null) {
            throw new EntityValidationException(UserRatingForm.class, "story should be presented");
        }

        if (form.getValue() == null) {
            throw new EntityValidationException(UserRatingForm.class, "value should be presented");
        }

        if (form.getValue() < 1) {
            throw new EntityValidationException(UserRatingForm.class, "value should be positive");
        }

        if (form.getValue() > 10) {
            throw new EntityValidationException(UserRatingForm.class, "value should be less that 10");
        }

        return form;
    }
}
