package org.grakovne.mds.server.rest.endpoints;

import org.apache.tika.Tika;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.StoryConverter;
import org.grakovne.mds.server.rest.forms.StoryForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.StoryResponse;
import org.grakovne.mds.server.services.StorySearchService;
import org.grakovne.mds.server.services.StoryService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static org.springframework.http.MediaType.valueOf;

@RestController
@RequestMapping("/api/v1/story")
public class StoryEndpoint {

    private final StoryService storyService;
    private final StorySearchService storySearchService;
    private final StoryConverter converter;

    public StoryEndpoint(StoryService storyService,
                         StorySearchService storySearchService,
                         StoryConverter converter) {

        this.storyService = storyService;
        this.storySearchService = storySearchService;
        this.converter = converter;
    }

    @GetMapping(value = "random")
    public ApiResponse<StoryResponse> findRandomStory() {
        Story story = storyService.findRandom();
        return converter.from(story);
    }

    @GetMapping(value = {"", "search"})
    public ApiResponse<Page<StoryResponse>> findAll(
        @RequestParam() Map<String, String> params,
        @AuthenticationPrincipal User user) {

        if (null != user) {
            params.put("userId", String.valueOf(user.getId()));
        } else {
            params.replace("listenedType", "both");
            params.remove("userId");
        }

        return converter.from(storySearchService.findStory(params));
    }

    @GetMapping(value = "{id}")
    public ApiResponse<StoryResponse> findOne(
        @PathVariable Integer id) {
        return converter.from(storyService.findOne(id));
    }

    @GetMapping(value = "{id}/stream")
    public ResponseEntity findStream(@PathVariable Integer id,
                                     @RequestParam(value = "offset", required = false, defaultValue = "0")
                                         int offset) throws IOException {

        File storyFile = storyService.findStoryAudio(id);
        InputStream storyStream = storyService.findStoryStream(id, offset);

        return ResponseEntity
            .ok()
            .contentType(valueOf(new Tika().detect(storyFile)))
            .contentLength(storyStream.available())
            .body(new InputStreamResource(storyStream));
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<StoryResponse> createOne(@RequestBody StoryForm form) {
        return converter.from(storyService.createOne(form));
    }

}
