package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.rest.converters.BookmarkConverter;
import org.grakovne.mds.server.rest.forms.BookmarkForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.BookmarkResponse;
import org.grakovne.mds.server.rest.validation.BookmarkValidation;
import org.grakovne.mds.server.services.BookmarkService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class BookmarkEndpoint {

    private final BookmarkService bookmarkService;
    private final BookmarkConverter converter;
    private final BookmarkValidation bookmarkValidation;

    public BookmarkEndpoint(BookmarkService bookmarkService,
                            BookmarkConverter converter,
                            BookmarkValidation bookmarkValidation) {

        this.bookmarkService = bookmarkService;
        this.converter = converter;
        this.bookmarkValidation = bookmarkValidation;
    }

    @GetMapping(value = "story/{id}/bookmark")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<List<BookmarkResponse>> findAllForStory(
        @PathVariable Integer id,
        @AuthenticationPrincipal User user) {

        return converter.from(bookmarkService.findAllForUser(id, user.getId()));
    }

    @GetMapping(value = "story/{id}/bookmark/last")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<BookmarkResponse> findLast(
        @PathVariable Integer id,
        @AuthenticationPrincipal User user) {

        return converter.from(bookmarkService
            .findLast(id, user)
            .orElseThrow(() -> new EntityNotFoundException(Bookmark.class)));
    }

    @PostMapping(value = "story/{id}/bookmark")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<BookmarkResponse> createOne(
        @RequestBody BookmarkForm form,
        @PathVariable Integer id,
        @AuthenticationPrincipal User user) {

        form.setStoryId(id);
        return converter.from(bookmarkService.createOne(id, bookmarkValidation.validate(form), user));
    }

    @DeleteMapping(value = "bookmark/{id}")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse deleteOne(
        @PathVariable Integer id,
        @AuthenticationPrincipal User user) {

        bookmarkService.deleteOne(id, user.getId());
        return new ApiResponse("Story has been deleted");
    }
}
