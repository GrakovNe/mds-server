package org.grakovne.mds.server.rest.handlers;

import org.grakovne.mds.server.exceptons.EntityException;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ControllerAdvice
public class EntityExceptionHandler {

    @ExceptionHandler(EntityException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public ApiResponse entityExceptionHandler(EntityException ex) {
        return new ApiResponse(ex.getMessage());
    }
}
