package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.StoryConverter;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.StoryResponse;
import org.grakovne.mds.server.services.RecentStoryService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/story")
public class RecentStoryEndpoint {

    private final RecentStoryService recentStoryService;
    private final StoryConverter converter;

    public RecentStoryEndpoint(RecentStoryService recentStoryService,
                               StoryConverter converter) {

        this.recentStoryService = recentStoryService;
        this.converter = converter;
    }

    @GetMapping(value = "recent")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<Page<StoryResponse>> findAllForUser(
        @AuthenticationPrincipal User user,
        @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        return converter.from(recentStoryService.findAllForUser(user, pageNumber));
    }

    @PostMapping(value = "{id}/recent")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<StoryResponse> createOne(@PathVariable Integer id,
                                                @AuthenticationPrincipal User user) {
        return converter.from(recentStoryService.createOne(user, id).getStory());
    }

    @DeleteMapping(value = "{id}/recent")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<StoryResponse> deleteOne(@PathVariable Integer id,
                                                @AuthenticationPrincipal User user) {
        recentStoryService.deleteOne(user, id);
        return new ApiResponse<>("Removed from recent");
    }
}
