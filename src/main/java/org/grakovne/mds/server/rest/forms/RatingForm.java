package org.grakovne.mds.server.rest.forms;

/**
 * Should be used fot initial story import only.
 */
public class RatingForm {

    private Double value;
    private Integer voters;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getVoters() {
        return voters;
    }

    public void setVoters(Integer voters) {
        this.voters = voters;
    }
}
