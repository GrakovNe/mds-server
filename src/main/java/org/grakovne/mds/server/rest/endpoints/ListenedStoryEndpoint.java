package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.ListenedStoryConverter;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.ListenedStoryResponse;
import org.grakovne.mds.server.services.ListenedStoryService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/story")
public class ListenedStoryEndpoint {

    private final ListenedStoryService listenedStoryService;
    private final ListenedStoryConverter converter;

    public ListenedStoryEndpoint(ListenedStoryService listenedStoryService,
                                 ListenedStoryConverter converter) {

        this.listenedStoryService = listenedStoryService;
        this.converter = converter;
    }

    @PostMapping(value = "{storyId}/listen")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<ListenedStoryResponse> createOne(
        @PathVariable Integer storyId,
        @AuthenticationPrincipal User user) {

        return converter.from(listenedStoryService.createOne(storyId, user));
    }

    @DeleteMapping(value = "{storyId}/listen")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse unListenStory(
        @PathVariable Integer storyId,
        @AuthenticationPrincipal User user) {

        listenedStoryService.deleteOne(storyId, user);
        return new ApiResponse("Story has been deleted from listened");
    }
}
