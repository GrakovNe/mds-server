package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.rest.converters.AssetConverter;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.AssetResponse;
import org.grakovne.mds.server.services.AssetService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/asset")
public class AssetEndpoint {

    private final AssetService assetService;
    private final AssetConverter converter;

    public AssetEndpoint(AssetService assetService,
                         AssetConverter converter) {

        this.assetService = assetService;
        this.converter = converter;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<AssetResponse> createOne(@RequestParam("file") MultipartFile file) {
        return converter.from(assetService.createOne(file));
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<AssetResponse> findOne(@PathVariable("id") Integer id) {
        return converter.from(assetService.findOne(id));
    }

}
