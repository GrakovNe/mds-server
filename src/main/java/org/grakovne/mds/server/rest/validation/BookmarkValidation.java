package org.grakovne.mds.server.rest.validation;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.exceptons.EntityValidationException;
import org.grakovne.mds.server.rest.forms.BookmarkForm;
import org.grakovne.mds.server.services.StoryService;
import org.springframework.stereotype.Service;

@Service
public class BookmarkValidation implements ValidationService<BookmarkForm> {

    private final StoryService storyService;

    public BookmarkValidation(StoryService storyService) {
        this.storyService = storyService;
    }

    @Override
    public BookmarkForm validate(BookmarkForm form) {
        if (null == form) {
            throw new EntityValidationException(Bookmark.class, "story bookmark is null");
        }

        if (null == form.getStoryId()) {
            throw new EntityValidationException(Bookmark.class, "story bookmark must have positive timestamp");
        }

        if (form.getTimestamp() == 0) {
            throw new EntityValidationException(Bookmark.class, "story bookmark must have positive timestamp");
        }

        if (form.getTimestamp().equals(storyService.findOne(form.getStoryId()).getLength())) {
            throw new EntityValidationException(Bookmark.class, "story bookmark can't be set in end of story");
        }

        return form;
    }
}
