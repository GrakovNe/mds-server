package org.grakovne.mds.server.rest.forms;

public class UserRatingForm implements MdsForm {

    private Double value;
    private Integer storyId;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }
}
