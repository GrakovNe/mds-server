package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.CommentConverter;
import org.grakovne.mds.server.rest.forms.CommentForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.CommentResponse;
import org.grakovne.mds.server.rest.validation.CommentValidation;
import org.grakovne.mds.server.services.CommentService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentEndpoint {

    private final CommentService commentService;
    private final CommentConverter converter;
    private final CommentValidation commentValidation;

    public CommentEndpoint(CommentService commentService,
                           CommentConverter converter,
                           CommentValidation commentValidation) {

        this.commentService = commentService;
        this.converter = converter;
        this.commentValidation = commentValidation;
    }

    @GetMapping(value = "/api/v1/story/{id}/comment")
    public ApiResponse<Page<CommentResponse>> findAllForStory(
        @PathVariable Integer id,
        @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        return converter.from(commentService.findAll(id, pageNumber));
    }

    @PostMapping(value = "/api/v1/story/{id}/comment")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<CommentResponse> createOne(
        @PathVariable Integer id,
        @RequestBody CommentForm form,
        @AuthenticationPrincipal User user) {

        return converter.from(commentService.createOne(user, id, commentValidation.validate(form)));
    }

    @DeleteMapping(value = "/api/v1/comment/{id}")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse deleteOne(
        @PathVariable Integer id,
        @AuthenticationPrincipal User user) {

        commentService.deleteOne(id, user);
        return new ApiResponse("Comment has been deleted");
    }
}
