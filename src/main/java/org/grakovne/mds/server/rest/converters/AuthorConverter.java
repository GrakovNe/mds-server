package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.Author;
import org.grakovne.mds.server.rest.responses.AuthorResponse;
import org.springframework.stereotype.Service;

@Service
public class AuthorConverter implements MdsConverter<Author, AuthorResponse> {

    @Override
    public AuthorResponse getBlankResponse() {
        return new AuthorResponse();
    }

    @Override
    public AuthorResponse feedWith(Author author, AuthorResponse response) {
        response.setId(author.getId());
        response.setName(author.getName());

        return response;
    }
}
