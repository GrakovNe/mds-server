package org.grakovne.mds.server.rest.forms;

import java.util.List;

public class StoryForm {

    private String title;
    private List<String> authors;
    private Integer year;
    private RatingForm rating;
    private String annotation;

    private Integer assetId;
    private Integer coverId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public RatingForm getRating() {
        return rating;
    }

    public void setRating(RatingForm rating) {
        this.rating = rating;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public Integer getCoverId() {
        return coverId;
    }

    public void setCoverId(Integer coverId) {
        this.coverId = coverId;
    }

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }
}
