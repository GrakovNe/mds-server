package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.Cover;
import org.grakovne.mds.server.rest.responses.CoverResponse;
import org.springframework.stereotype.Service;

@Service
public class CoverConverter implements MdsConverter<Cover, CoverResponse> {

    @Override
    public CoverResponse getBlankResponse() {
        return new CoverResponse();
    }

    @Override
    public CoverResponse feedWith(Cover cover, CoverResponse response) {
        response.setId(cover.getId());
        response.setStoryId(cover.getStory().getId());

        return response;
    }
}
