package org.grakovne.mds.server.rest.validation;

import com.google.common.base.Strings;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.exceptons.EntityValidationException;
import org.grakovne.mds.server.rest.forms.UserForm;
import org.springframework.stereotype.Service;

@Service
public class UserValidation implements ValidationService<UserForm> {
    @Override
    public UserForm validate(UserForm form) {
        if (null == form) {
            throw new EntityValidationException(UserForm.class, "user is null");
        }

        if (Strings.isNullOrEmpty(form.getUsername())) {
            throw new EntityValidationException(User.class, "user must have username");
        }

        if (Strings.isNullOrEmpty(form.getPassword())) {
            throw new EntityValidationException(User.class, "user must have password");
        }

        return form;
    }
}
