package org.grakovne.mds.server.rest.responses;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserMetricResponse {

    private Long stories;
    private Long authors;
    private Long comments;
    private Long ratedStories;
    private Long listenedStories;
    private Long listenedTime;
    private Long finishedStories;

    public Long getStories() {
        return stories;
    }

    public void setStories(Long stories) {
        this.stories = stories;
    }

    public Long getAuthors() {
        return authors;
    }

    public void setAuthors(Long authors) {
        this.authors = authors;
    }

    public Long getRatedStories() {
        return ratedStories;
    }

    public void setRatedStories(Long ratedStories) {
        this.ratedStories = ratedStories;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public Long getListenedStories() {
        return listenedStories;
    }

    public void setListenedStories(Long listenedStories) {
        this.listenedStories = listenedStories;
    }

    public Long getListenedTime() {
        return listenedTime;
    }

    public void setListenedTime(Long listenedTime) {
        this.listenedTime = listenedTime;
    }

    public Long getFinishedStories() {
        return finishedStories;
    }

    public void setFinishedStories(Long finishedStories) {
        this.finishedStories = finishedStories;
    }
}
