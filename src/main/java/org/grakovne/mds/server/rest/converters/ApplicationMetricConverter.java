package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.ApplicationMetric;
import org.grakovne.mds.server.rest.responses.ApplicationMetricResponse;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

@Service
public class ApplicationMetricConverter implements MdsConverter<ApplicationMetric, ApplicationMetricResponse> {

    @Override
    public ApplicationMetricResponse getBlankResponse() {
        return new ApplicationMetricResponse();
    }

    @Override
    public ApplicationMetricResponse feedWith(ApplicationMetric metric, ApplicationMetricResponse response) {
        response.setListenedTime(toTime(metric.getListenedTime()));
        response.setUsers(metric.getUsers());
        response.setStories(metric.getStories());
        response.setAuthors(metric.getAuthors());
        response.setBookmarks(metric.getBookmarks());
        response.setComments(metric.getComments());
        response.setListenedStories(metric.getListenedStories());
        response.setRecentStories(metric.getRecentStories());
        response.setRatedStories(metric.getRatedStories());
        response.setUserRatings(metric.getUserRatings());
        response.setCreatedDateTime(metric.getCreateDateTime());

        return response;
    }

    private String toTime(Long seconds) {
        long day = TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds) - (day * 24);
        long minute = TimeUnit.SECONDS.toMinutes(seconds) - (TimeUnit.SECONDS.toHours(seconds) * 60);
        long second = TimeUnit.SECONDS.toSeconds(seconds) - (TimeUnit.SECONDS.toMinutes(seconds) * 60);
        return day + " days " + hours + ":" + minute + ":" + second;
    }
}
