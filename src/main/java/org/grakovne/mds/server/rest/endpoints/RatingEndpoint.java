package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.StoryConverter;
import org.grakovne.mds.server.rest.forms.UserRatingForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.StoryResponse;
import org.grakovne.mds.server.rest.validation.UserRatingValidation;
import org.grakovne.mds.server.services.UserRatingService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/story/{id}/rating")
public class RatingEndpoint {

    private final UserRatingService userRatingService;
    private final StoryConverter converter;
    private final UserRatingValidation validation;

    public RatingEndpoint(UserRatingService userRatingService,
                          StoryConverter converter, UserRatingValidation validation) {
        this.userRatingService = userRatingService;
        this.converter = converter;
        this.validation = validation;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public ApiResponse<StoryResponse> createOne(@AuthenticationPrincipal User user,
                                                @RequestBody UserRatingForm form,
                                                @PathVariable Integer id) {
        form.setStoryId(id);
        return converter.from(userRatingService.createOne(user, validation.validate(form)).getStory());
    }
}
