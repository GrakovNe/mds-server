package org.grakovne.mds.server.rest.converters;

import org.apache.commons.math3.util.Precision;
import org.grakovne.mds.server.domain.Author;
import org.grakovne.mds.server.domain.Rating;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.responses.StoryResponse;
import org.grakovne.mds.server.services.BookmarkService;
import org.grakovne.mds.server.services.UserRatingService;
import org.springframework.stereotype.Service;

import java.util.function.Consumer;
import java.util.stream.Collectors;

import static java.util.Optional.ofNullable;

@Service
public class StoryConverter implements MdsConverter<Story, StoryResponse> {

    private final BookmarkService bookmarkService;
    private final UserRatingService userRatingService;

    public StoryConverter(BookmarkService bookmarkService, UserRatingService userRatingService) {
        this.bookmarkService = bookmarkService;
        this.userRatingService = userRatingService;
    }

    @Override
    public StoryResponse getBlankResponse() {
        return new StoryResponse();
    }

    @Override
    public StoryResponse feedWith(Story story, StoryResponse response) {

        response.setId(story.getId());
        response.setTitle(story.getTitle());
        response.setYear(story.getYear());
        response.setAuthors(story
            .getAuthors()
            .stream()
            .map(Author::getName)
            .collect(Collectors.joining(", "))
        );

        ofNullable(story.getRating()).ifPresent(feedWithRating(response));
        ofNullable(story.getCover()).ifPresent(c -> response.setCoverId(story.getCover().getId()));

        response.setAnnotation(story.getAnnotation());
        response.setFileSize(story.getFileSize());
        response.setFileQuality(story.getFileQuality());
        response.setLength(story.getLength());

        return response;
    }

    @Override
    public StoryResponse feedWithUserData(Story story, StoryResponse response, User user) {
        bookmarkService
            .findLast(story.getId(), user)
            .ifPresent(bookmark -> response.setProgress(bookmark.getTimestamp()));

        userRatingService.findOne(user, story.getId()).ifPresent(feedWithRating(response));

        return response;
    }

    private Consumer<Rating> feedWithRating(StoryResponse response) {
        return r -> response.setRating(Precision.round(r.getValue(), 2));
    }
}
