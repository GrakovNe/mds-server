package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.rest.responses.AssetResponse;
import org.springframework.stereotype.Service;

@Service
public class AssetConverter implements MdsConverter<Asset, AssetResponse> {

    @Override
    public AssetResponse getBlankResponse() {
        return new AssetResponse();
    }

    @Override
    public AssetResponse feedWith(Asset asset, AssetResponse response) {
        response.setId(asset.getId());
        response.setFileName(asset.getFileName());
        response.setHash(asset.getHash());
        response.setCreatedDateTime(asset.getCreatedDateTime());

        return response;
    }
}
