package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public interface MdsConverter<Entity, Response> {

    Response feedWith(Entity entity, Response response);

    Response getBlankResponse();

    default Response feedWithUserData(Entity entity, Response response, User user) {
        return response;
    }

    default ApiResponse<Response> from(Entity entity) {
        return null == entity
            ? new ApiResponse()
            : new ApiResponse<>(toResponse(entity));
    }

    default ApiResponse<List<Response>> from(Collection<Entity> collection) {
        return new ApiResponse<>(collection.stream().map(this::toResponse).collect(Collectors.toList()));
    }

    default ApiResponse<Page<Response>> from(Page<Entity> page) {
        return new ApiResponse<>(page.map(this::toResponse));
    }

    private Response toResponse(Entity entity) {
        Response response = feedWith(entity, getBlankResponse());
        return findTargetUser()
            .map(user -> feedWithUserData(entity, response, user))
            .orElse(response);
    }

    private Optional<User> findTargetUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof User) {
            return Optional.of((User) principal);
        } else {
            return Optional.empty();
        }
    }
}
