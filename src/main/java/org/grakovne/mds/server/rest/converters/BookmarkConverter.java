package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.rest.responses.BookmarkResponse;
import org.springframework.stereotype.Service;

@Service
public class BookmarkConverter implements MdsConverter<Bookmark, BookmarkResponse> {

    @Override
    public BookmarkResponse getBlankResponse() {
        return new BookmarkResponse();
    }

    @Override
    public BookmarkResponse feedWith(Bookmark bookmark, BookmarkResponse response) {
        response.setId(bookmark.getId());
        response.setStoryId(bookmark.getStory().getId());
        response.setCreateDateTime(bookmark.getCreateDateTime());
        response.setTimestamp(bookmark.getTimestamp());

        return response;
    }
}
