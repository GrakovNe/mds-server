package org.grakovne.mds.server.rest.validation;

import org.grakovne.mds.server.rest.forms.MdsForm;

public interface ValidationService<Form extends MdsForm> {

    Form validate(Form form);
}
