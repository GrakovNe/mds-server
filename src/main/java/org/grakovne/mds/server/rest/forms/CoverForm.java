package org.grakovne.mds.server.rest.forms;

public class CoverForm {
    private Integer assetId;

    public Integer getAssetId() {
        return assetId;
    }

    public void setAssetId(Integer assetId) {
        this.assetId = assetId;
    }
}
