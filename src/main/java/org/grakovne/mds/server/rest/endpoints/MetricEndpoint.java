package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.ApplicationMetricConverter;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.ApplicationMetricResponse;
import org.grakovne.mds.server.rest.responses.UserMetricResponse;
import org.grakovne.mds.server.services.ApplicationMetricService;
import org.grakovne.mds.server.services.UserMetricService;
import org.grakovne.mds.server.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class MetricEndpoint {

    private final ApplicationMetricService applicationMetricService;
    private final UserMetricService userMetricService;
    private final UserService userService;
    private final ApplicationMetricConverter converter;

    public MetricEndpoint(ApplicationMetricService applicationMetricService,
                          UserMetricService userMetricService,
                          UserService userService,
                          ApplicationMetricConverter converter) {

        this.applicationMetricService = applicationMetricService;
        this.userMetricService = userMetricService;
        this.userService = userService;
        this.converter = converter;
    }

    @GetMapping("metrics/application")
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<Page<ApplicationMetricResponse>> generateApplicationMetrics(
        @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {
        return converter.from(applicationMetricService.findAll(pageNumber));
    }

    @GetMapping("user/me/metrics")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<UserMetricResponse> generateUserMetrics(@AuthenticationPrincipal User user) {
        return new ApiResponse<>(userMetricService.createInstant(user));
    }


    @GetMapping("user/{id}/metrics")
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<UserMetricResponse> generateUserMetric(@PathVariable Integer id) {
        return new ApiResponse<>(userMetricService.createInstant(userService.findOne(id)));
    }
}
