package org.grakovne.mds.server.rest.endpoints;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.rest.converters.UserConverter;
import org.grakovne.mds.server.rest.forms.UserForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.UserResponse;
import org.grakovne.mds.server.rest.validation.UserValidation;
import org.grakovne.mds.server.services.BookmarkService;
import org.grakovne.mds.server.services.ListenedStoryService;
import org.grakovne.mds.server.services.RecentStoryService;
import org.grakovne.mds.server.services.UserRatingService;
import org.grakovne.mds.server.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserEndpoint {

    private final UserService userService;
    private final RecentStoryService recentStoryService;
    private final BookmarkService bookmarkService;
    private final ListenedStoryService listenedStoryService;
    private final UserRatingService userRatingService;
    private final UserConverter converter;
    private final UserValidation userValidation;

    public UserEndpoint(UserService userService,
                        RecentStoryService recentStoryService,
                        BookmarkService bookmarkService,
                        ListenedStoryService listenedStoryService,
                        UserRatingService userRatingService,
                        UserConverter converter,
                        UserValidation userValidation) {

        this.userService = userService;
        this.recentStoryService = recentStoryService;
        this.bookmarkService = bookmarkService;
        this.listenedStoryService = listenedStoryService;
        this.userRatingService = userRatingService;
        this.converter = converter;
        this.userValidation = userValidation;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<Page<UserResponse>> findAll(
        @RequestParam(required = false, defaultValue = "0") Integer pageNumber) {

        return converter.from(userService.findAll(pageNumber));
    }

    @GetMapping(value = "me")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse<UserResponse> findMe(@AuthenticationPrincipal User user) {
        return converter.from(user);
    }

    @PostMapping
    public ApiResponse<UserResponse> createOne(@RequestBody UserForm userForm) {
        return converter.from(userService.createOne(userValidation.validate(userForm)));
    }

    @DeleteMapping(value = "me")
    @PreAuthorize("hasRole('USER')")
    public ApiResponse forgetMe(@AuthenticationPrincipal User user) {
        recentStoryService.deleteAll(user);
        bookmarkService.deleteAll(user);
        listenedStoryService.deleteAll(user);
        userRatingService.deleteAll(user);
        return new ApiResponse("User data was cleared");
    }

}
