package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.ListenedStory;
import org.grakovne.mds.server.rest.responses.ListenedStoryResponse;
import org.springframework.stereotype.Service;

@Service
public class ListenedStoryConverter implements MdsConverter<ListenedStory, ListenedStoryResponse> {

    @Override
    public ListenedStoryResponse getBlankResponse() {
        return new ListenedStoryResponse();
    }

    @Override
    public ListenedStoryResponse feedWith(ListenedStory listenedStory, ListenedStoryResponse response) {
        response.setId(listenedStory.getId());
        response.setStoryId(listenedStory.getStory().getId());
        response.setCreateDateTime(listenedStory.getCreateDateTime());

        return response;
    }
}
