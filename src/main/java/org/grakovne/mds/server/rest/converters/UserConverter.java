package org.grakovne.mds.server.rest.converters;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.UserRole;
import org.grakovne.mds.server.rest.responses.UserResponse;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class UserConverter implements MdsConverter<User, UserResponse> {

    @Override
    public UserResponse getBlankResponse() {
        return new UserResponse();
    }

    @Override
    public UserResponse feedWith(User user, UserResponse response) {
        response.setId(user.getId());
        response.setUsername(user.getUsername());
        response.setPassword(user.getPassword());
        response.setExpired(!user.isAccountNonExpired());
        response.setCredentialsExpired(!user.isCredentialsNonExpired());
        response.setLocked(!user.isAccountNonLocked());
        response.setEnabled(user.isEnabled());
        response.setRoles(user.getUserRoles().stream().map(UserRole::getName).collect(Collectors.toList()));

        return response;
    }
}
