package org.grakovne.mds.server.rest.endpoints;

import org.apache.tika.Tika;
import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.domain.Cover;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.rest.converters.CoverConverter;
import org.grakovne.mds.server.rest.forms.CoverForm;
import org.grakovne.mds.server.rest.responses.ApiResponse;
import org.grakovne.mds.server.rest.responses.CoverResponse;
import org.grakovne.mds.server.services.AssetService;
import org.grakovne.mds.server.services.CoverService;
import org.grakovne.mds.server.services.StoryService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;

@RestController
public class CoverEndpoint {

    private final StoryService storyService;
    private final AssetService assetService;
    private final CoverService coverService;
    private final CoverConverter converter;

    public CoverEndpoint(StoryService storyService,
                         AssetService assetService,
                         CoverService coverService,
                         CoverConverter converter) {

        this.storyService = storyService;
        this.assetService = assetService;
        this.coverService = coverService;
        this.converter = converter;
    }

    @GetMapping(value = "/api/v1/story/{id}/cover")
    public ResponseEntity findOne(@PathVariable Integer id) throws IOException {
        Asset asset = Optional
            .ofNullable(storyService.findOne(id).getCover())
            .orElseThrow(() -> new EntityNotFoundException(Cover.class))
            .getAsset();

        File file = assetService.findFile(asset);

        return ResponseEntity
            .ok()
            .contentType(MediaType.valueOf(new Tika().detect(file)))
            .body(new InputStreamResource(new FileInputStream(file)));
    }

    @PostMapping(value = "/api/v1/cover")
    @PreAuthorize("hasRole('ADMIN')")
    public ApiResponse<CoverResponse> createCover(@RequestBody CoverForm form) {
        Asset asset = assetService.findOne(form.getAssetId());
        return converter.from(coverService.createOne(asset));
    }
}
