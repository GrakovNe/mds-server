package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.exceptons.EntityException;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.repositories.StoryRepository;
import org.grakovne.mds.server.rest.forms.StoryForm;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.audio.mp3.MP3AudioHeader;
import org.jaudiotagger.audio.mp3.MP3File;
import org.jaudiotagger.tag.TagException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static java.util.stream.Collectors.toSet;

@Service
public class StoryService {

    private final StoryRepository storyRepository;

    private final RatingService ratingService;
    private final AssetService assetService;
    private final CoverService coverService;
    private final AuthorService authorService;

    public StoryService(StoryRepository storyRepository,
                        RatingService ratingService,
                        AssetService assetService,
                        CoverService coverService,
                        AuthorService authorService) {

        this.storyRepository = storyRepository;
        this.ratingService = ratingService;
        this.assetService = assetService;
        this.coverService = coverService;
        this.authorService = authorService;
    }

    public Story findOne(Integer id) {
        return storyRepository.findOneById(id).orElseThrow(() -> new EntityNotFoundException(Story.class));
    }

    public File findStoryAudio(Integer storyId) {
        return assetService.findFile(findOne(storyId).getAsset());
    }

    public InputStream findStoryStream(Integer id, Integer offset) {
        Story story = findOne(id);

        try {
            InputStream fileStream = new FileInputStream(findStoryAudio(id));

            Long bytesPerSecond = story.getFileSize() / story.getLength();
            Long skipBytes = offset * bytesPerSecond;

            if (skipBytes < story.getFileSize()) {
                fileStream.skip(skipBytes);
            }

            return fileStream;

        } catch (IOException e) {
            throw new EntityException(Story.class, "Audio file is invalid");
        }
    }

    public Story findRandom() {
        return storyRepository.findRandomStory();
    }

    public Story createOne(StoryForm form) {
        Story story = new Story();

        story.setTitle(form.getTitle());
        story.setYear(form.getYear());
        story.setAnnotation(form.getAnnotation());
        story.setAuthors(form.getAuthors().stream().map(authorService::createOne).collect(toSet()));

        story.setAsset(assetService.findOne(form.getAssetId()));

        if (null != form.getCoverId()) {
            story.setCover(coverService.findOne(form.getCoverId()));
        }

        feedWithAudioData(story);

        Story saved = storyRepository.save(story);

        if (null != form.getRating()) {
            ratingService.createOne(form.getRating(), saved);
        }

        return saved;
    }

    private void feedWithAudioData(Story story) {
        File assetFile = assetService.findFile(story.getAsset());

        try {
            MP3File mp3File = new MP3File(assetFile);
            MP3AudioHeader header = mp3File.getMP3AudioHeader();

            story.setFileQuality(Long.valueOf(header.getBitRate()));
            story.setFileSize(assetFile.length());
            story.setLength(header.getTrackLength());

        } catch (IOException | TagException | ReadOnlyFileException | InvalidAudioFrameException e) {
            throw new EntityException(Asset.class, "can't read mp3 file");
        }
    }
}
