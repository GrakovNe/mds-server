package org.grakovne.mds.server.services;

import org.apache.commons.lang.StringUtils;
import org.grakovne.mds.server.domain.Comment;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.exceptons.EntityException;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.providers.ConfigurationProvider;
import org.grakovne.mds.server.repositories.CommentRepository;
import org.grakovne.mds.server.rest.forms.CommentForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class CommentService {
    private final StoryService storyService;
    private final CommentRepository commentRepository;
    private final ConfigurationProvider configurationProvider;
    private final UserService userService;

    public CommentService(StoryService storyService,
                          CommentRepository commentRepository,
                          ConfigurationProvider configurationProvider,
                          UserService userService) {

        this.storyService = storyService;
        this.commentRepository = commentRepository;
        this.configurationProvider = configurationProvider;
        this.userService = userService;
    }

    public Comment createOne(User user, Integer storyId, CommentForm form) {
        Comment comment = new Comment();

        comment.setStory(storyService.findOne(storyId));
        comment.setCreatedDateTime(LocalDateTime.now());
        comment.setUser(user);
        comment.setMessage(form.getMessage());

        if (StringUtils.isEmpty(form.getUserName())) {
            comment.setUsername(user.getUsername());
        }

        return commentRepository.save(comment);
    }

    public void deleteOne(Integer commentId, User user) {
        Comment comment = commentRepository
            .findOneById(commentId)
            .orElseThrow(() -> new EntityNotFoundException(Comment.class));

        if (userService.isAdmin(user) || comment.getUser().getId().equals(user.getId())) {
            commentRepository.delete(comment);
        }

        throw new EntityException(Comment.class, "Comment isn't belong to user");
    }

    public Page<Comment> findAll(Integer storyId, Integer pageNumber) {
        return commentRepository.findAllByStoryOrderByCreatedDateTimeDesc(
            storyService.findOne(storyId),
            PageRequest.of(pageNumber, configurationProvider.getPageSize())
        );
    }
}
