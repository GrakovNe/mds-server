package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.domain.Cover;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.repositories.CoverRepository;
import org.springframework.stereotype.Service;

@Service
public class CoverService {

    private final CoverRepository coverRepository;

    public CoverService(CoverRepository coverRepository) {
        this.coverRepository = coverRepository;
    }

    public Cover findOne(Integer id) {
        return coverRepository.findOneById(id).orElseThrow(() -> new EntityNotFoundException(Cover.class));
    }

    public Cover createOne(Asset asset) {
        Cover cover = new Cover();
        cover.setAsset(asset);
        return coverRepository.save(cover);
    }
}
