package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.domain.ListenedStory;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.repositories.AuthorRepository;
import org.grakovne.mds.server.repositories.BookmarkRepository;
import org.grakovne.mds.server.repositories.ListenedStoryRepository;
import org.grakovne.mds.server.repositories.RecentStoryRepository;
import org.grakovne.mds.server.repositories.StoryRepository;
import org.grakovne.mds.server.repositories.UserRatingRepository;
import org.grakovne.mds.server.rest.responses.UserMetricResponse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class UserMetricService {
    private final StoryRepository storyRepository;
    private final AuthorRepository authorRepository;
    private final BookmarkRepository bookmarkRepository;
    private final ListenedStoryRepository listenedStoryRepository;
    private final RecentStoryRepository recentStoryRepository;
    private final UserRatingRepository userRatingRepository;

    public UserMetricService(StoryRepository storyRepository,
                             AuthorRepository authorRepository,
                             BookmarkRepository bookmarkRepository,
                             ListenedStoryRepository listenedStoryRepository,
                             RecentStoryRepository recentStoryRepository,
                             UserRatingRepository userRatingRepository) {

        this.storyRepository = storyRepository;
        this.authorRepository = authorRepository;
        this.bookmarkRepository = bookmarkRepository;
        this.listenedStoryRepository = listenedStoryRepository;
        this.recentStoryRepository = recentStoryRepository;
        this.userRatingRepository = userRatingRepository;
    }

    public UserMetricResponse createInstant(User user) {
        UserMetricResponse response = new UserMetricResponse();

        response.setAuthors(authorRepository.count());
        response.setStories(storyRepository.count());
        response.setFinishedStories(listenedStoryRepository.countAllByUser(user));
        response.setListenedStories(recentStoryRepository.countAllByUser(user));
        response.setRatedStories(userRatingRepository.countAllByUser(user));
        response.setListenedTime(getListenedTime(user));

        return response;
    }

    public Long getListenedTime(User user) {

        List<ListenedStory> listened = listenedStoryRepository.findAllByUser(user);

        List<Story> listenedStories = listened
            .stream()
            .map(ListenedStory::getStory)
            .collect(Collectors.toList());

        long listenedStoriesTime = listened
            .stream()
            .map(ListenedStory::getStory)
            .map(Story::getLength)
            .mapToLong(value -> value)
            .sum();

        Set<Object> seen = ConcurrentHashMap.newKeySet();
        long unlistenedStoriesTime = bookmarkRepository
            .findByUserOrderByCreateDateTimeDesc(user)
            .stream()
            .filter(t -> seen.add(t.getStory()))
            .filter(s -> !listenedStories.contains(s.getStory()))
            .map(Bookmark::getTimestamp)
            .mapToLong(value -> value)
            .sum();

        return listenedStoriesTime + unlistenedStoriesTime;
    }
}
