package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.RecentStory;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.enums.State;
import org.grakovne.mds.server.providers.ConfigurationProvider;
import org.grakovne.mds.server.repositories.RecentStoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

import static org.grakovne.mds.server.domain.enums.State.ACTIVE;

@Service
public class RecentStoryService {

    private final RecentStoryRepository recentStoryRepository;
    private final StoryService storyService;
    private final ConfigurationProvider configurationProvider;

    public RecentStoryService(RecentStoryRepository recentStoryRepository,
                              StoryService storyService,
                              ConfigurationProvider configurationProvider) {
        this.recentStoryRepository = recentStoryRepository;
        this.storyService = storyService;
        this.configurationProvider = configurationProvider;
    }

    public RecentStory createOne(User user, Integer id) {
        Story story = storyService.findOne(id);

        RecentStory recentStory = recentStoryRepository
            .findByStoryAndUser(story, user).orElseGet(() -> {
                RecentStory rs = new RecentStory();
                rs.setStory(story);
                rs.setUser(user);
                rs.setCreateDateTime(LocalDateTime.now());
                return rs;
            });

        recentStory.setState(ACTIVE);
        return recentStoryRepository.save(recentStory);
    }

    public void deleteOne(User user, Integer id) {
        Story story = storyService.findOne(id);
        recentStoryRepository.findByStoryAndUser(story, user).ifPresent(s -> {
            s.setState(State.REMOVED);
            recentStoryRepository.save(s);
        });
    }

    public void deleteAll(User user) {
        recentStoryRepository.deleteAll(recentStoryRepository.findAllByUser(user));
    }

    public Page<Story> findAllForUser(User user, Integer page) {
        return recentStoryRepository
            .findAllByUserAndStateOrderByCreateDateTimeDesc(user,
                ACTIVE,
                PageRequest.of(page, configurationProvider.getPageSize()))
            .map(RecentStory::getStory);
    }

}
