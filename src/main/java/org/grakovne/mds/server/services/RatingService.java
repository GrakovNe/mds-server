package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Rating;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.UserRating;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.repositories.RatingRepository;
import org.grakovne.mds.server.repositories.StoryRepository;
import org.grakovne.mds.server.rest.forms.RatingForm;
import org.springframework.stereotype.Service;

@Service
public class RatingService {

    private final RatingRepository ratingRepository;
    private final StoryRepository storyRepository;

    public RatingService(RatingRepository ratingRepository, StoryRepository storyRepository) {
        this.ratingRepository = ratingRepository;
        this.storyRepository = storyRepository;
    }

    public Rating createOne(RatingForm form, Story story) {
        Rating rating = new Rating();
        rating.setStory(story);
        rating.setValue(form.getValue());
        rating.setVoters(form.getVoters());

        Rating saved = ratingRepository.save(rating);
        story.setRating(saved);
        storyRepository.save(story);
        return saved;
    }

    public Rating findOne(Integer storyId) {
        return storyRepository
            .findOneById(storyId)
            .orElseThrow(() -> new EntityNotFoundException(Story.class))
            .getRating();
    }

    public void invokeInfluence(UserRating userRating) {
        Rating rating = findOne(userRating.getStory().getId());

        if (null == rating) {
            RatingForm ratingForm = new RatingForm();
            ratingForm.setValue(userRating.getValue());
            ratingForm.setVoters(1);
            createOne(ratingForm, userRating.getStory());
            return;
        }

        double valueSum = rating.getValue() * rating.getVoters() + userRating.getValue();
        int voterSum = rating.getVoters() + 1;

        rating.setVoters(voterSum);
        rating.setValue(valueSum / voterSum);
        ratingRepository.save(rating);
    }

    public void revokeInfluence(UserRating userRating) {
        Rating rating = findOne(userRating.getStory().getId());

        if (null == rating) {
            return;
        }

        double valueSum = rating.getValue() * rating.getVoters() - userRating.getValue();
        int voterSum = rating.getVoters() - 1;

        if (voterSum == 0) {
            ratingRepository.delete(rating);
            return;
        }

        rating.setVoters(voterSum);
        rating.setValue(valueSum / voterSum);
        ratingRepository.save(rating);
    }
}
