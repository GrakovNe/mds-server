package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Rating;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.UserRating;
import org.grakovne.mds.server.repositories.UserRatingRepository;
import org.grakovne.mds.server.rest.forms.UserRatingForm;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserRatingService {

    private final UserRatingRepository userRatingRepository;
    private final StoryService storyService;
    private final RatingService ratingService;

    public UserRatingService(UserRatingRepository userRatingRepository,
                             StoryService storyService,
                             RatingService ratingService) {

        this.userRatingRepository = userRatingRepository;
        this.storyService = storyService;
        this.ratingService = ratingService;
    }

    public Optional<Rating> findOne(User user, Integer storyId) {
        return userRatingRepository
            .findByUserAndStory(user, storyService.findOne(storyId))
            .map(this::fromUserRating);
    }

    public Rating createOne(User user, UserRatingForm form) {
        userRatingRepository
            .findByUserAndStory(user, storyService.findOne(form.getStoryId()))
            .ifPresent(userRating -> deleteOne(user, form));

        UserRating rating = new UserRating();

        rating.setStory(storyService.findOne(form.getStoryId()));
        rating.setUser(user);
        rating.setValue(form.getValue());
        rating.setCreatedDateTime(LocalDateTime.now());

        UserRating saved = userRatingRepository.save(rating);
        ratingService.invokeInfluence(saved);

        return fromUserRating(saved);
    }

    public void deleteOne(User user, UserRatingForm form) {
        userRatingRepository
            .findByUserAndStory(user, storyService.findOne(form.getStoryId()))
            .ifPresent(t -> {
                ratingService.revokeInfluence(t);
                userRatingRepository.delete(t);
            });
    }

    private Rating fromUserRating(UserRating userRating) {
        Rating rating = new Rating();
        rating.setStory(userRating.getStory());
        rating.setValue(userRating.getValue());
        return rating;
    }

    public void deleteAll(User user) {
        userRatingRepository.findAllByUser(user).forEach(userRating -> {
            ratingService.revokeInfluence(userRating);
            userRatingRepository.delete(userRating);
        });
    }
}
