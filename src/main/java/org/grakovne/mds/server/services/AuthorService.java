package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Author;
import org.grakovne.mds.server.repositories.AuthorRepository;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {
    private final AuthorRepository authorRepository;


    public AuthorService(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    public Author createOne(String name) {
        return authorRepository.findByName(name).orElseGet(() -> {
            Author author = new Author();
            author.setName(name);
            return authorRepository.save(author);
        });
    }
}
