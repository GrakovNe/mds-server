package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.exceptons.EntityException;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.providers.FileProvider;
import org.grakovne.mds.server.repositories.AssetRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class AssetService {

    private final AssetRepository assetRepository;
    private final FileProvider fileProvider;

    public AssetService(AssetRepository assetRepository,
                        FileProvider provider) {
        this.assetRepository = assetRepository;
        this.fileProvider = provider;
    }

    public Asset createOne(MultipartFile file) {
        try {

            File temp = new File(UUID.randomUUID().toString());
            boolean newFile = temp.createNewFile();
            FileOutputStream fos = new FileOutputStream(temp);
            fos.write(file.getBytes());
            fos.close();

            Asset asset = createOne(temp);
            temp.delete();
            return asset;
        } catch (IOException e) {
            throw new EntityException(Asset.class, "can't create");
        }
    }

    public File findFile(Asset asset) {
        return fileProvider.getFile(asset.getFileName());
    }

    public Asset findOne(Integer id) {
        return assetRepository
            .findById(id)
            .filter(Asset -> Asset
                .getHash()
                .equalsIgnoreCase(hash(fileProvider.getFile(Asset.getFileName()))))
            .orElseThrow(() -> new EntityNotFoundException(Asset.class));
    }

    private Asset createOne(File content) {
        return assetRepository
            .findByHash(hash(content))
            .orElseGet(() -> {
                File savedContent = fileProvider.uploadFile(content);

                try {
                    Asset entity = new Asset();

                    entity = setCreatedDateTime(entity);
                    entity = setHash(entity, savedContent);
                    entity = setFileName(entity, savedContent);

                    return assetRepository.save(entity);
                } catch (RuntimeException ex) {
                    fileProvider.deleteFile(savedContent);
                    throw ex;
                }
            });
    }

    private Asset setFileName(Asset entity, File savedFile) {
        entity.setFileName(savedFile.getName());
        return entity;
    }

    private Asset setHash(Asset entity, File savedFile) {
        entity.setHash(hash(savedFile));
        return entity;
    }

    private Asset setCreatedDateTime(Asset entity) {
        entity.setCreatedDateTime(LocalDateTime.now());
        return entity;
    }

    private String hash(File file) {
        try {
            return DigestUtils.md5DigestAsHex(new FileInputStream(file));
        } catch (IOException e) {
            throw new RuntimeException("Can't hash file.");
        }
    }
}
