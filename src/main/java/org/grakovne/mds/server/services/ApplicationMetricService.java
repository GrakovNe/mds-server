package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.ApplicationMetric;
import org.grakovne.mds.server.providers.ConfigurationProvider;
import org.grakovne.mds.server.repositories.ApplicationMetricRepository;
import org.grakovne.mds.server.repositories.AuthorRepository;
import org.grakovne.mds.server.repositories.BookmarkRepository;
import org.grakovne.mds.server.repositories.CommentRepository;
import org.grakovne.mds.server.repositories.ListenedStoryRepository;
import org.grakovne.mds.server.repositories.RatingRepository;
import org.grakovne.mds.server.repositories.RecentStoryRepository;
import org.grakovne.mds.server.repositories.StoryRepository;
import org.grakovne.mds.server.repositories.UserRatingRepository;
import org.grakovne.mds.server.repositories.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ApplicationMetricService {

    private final UserRepository userRepository;
    private final StoryRepository storyRepository;
    private final AuthorRepository authorRepository;
    private final BookmarkRepository bookmarkRepository;
    private final CommentRepository commentRepository;
    private final ListenedStoryRepository listenedStoryRepository;
    private final RecentStoryRepository recentStoryRepository;
    private final UserRatingRepository userRatingRepository;
    private final RatingRepository ratingRepository;
    private final ApplicationMetricRepository applicationMetricRepository;
    private final UserMetricService userMetricService;
    private final ConfigurationProvider configurationProvider;

    public ApplicationMetricService(UserRepository userRepository,
                                    StoryRepository storyRepository,
                                    AuthorRepository authorRepository,
                                    BookmarkRepository bookmarkRepository,
                                    CommentRepository commentRepository,
                                    ListenedStoryRepository listenedStoryRepository,
                                    RecentStoryRepository recentStoryRepository,
                                    UserRatingRepository userRatingRepository,
                                    RatingRepository ratingRepository,
                                    ApplicationMetricRepository applicationMetricRepository,
                                    UserMetricService userMetricService,
                                    ConfigurationProvider configurationProvider) {

        this.userRepository = userRepository;
        this.storyRepository = storyRepository;
        this.authorRepository = authorRepository;
        this.bookmarkRepository = bookmarkRepository;
        this.commentRepository = commentRepository;
        this.listenedStoryRepository = listenedStoryRepository;
        this.recentStoryRepository = recentStoryRepository;
        this.userRatingRepository = userRatingRepository;
        this.ratingRepository = ratingRepository;
        this.applicationMetricRepository = applicationMetricRepository;
        this.userMetricService = userMetricService;
        this.configurationProvider = configurationProvider;
    }

    public Page<ApplicationMetric> findAll(Integer page) {
        return applicationMetricRepository
            .findByOrderByCreateDateTimeDesc(PageRequest.of(page, configurationProvider.getPageSize()));
    }

    public ApplicationMetric createOne() {
        ApplicationMetric metric = new ApplicationMetric();

        metric.setCreateDateTime(LocalDateTime.now());
        metric.setAuthors(authorRepository.count());
        metric.setUsers(userRepository.count());
        metric.setStories(storyRepository.count());
        metric.setBookmarks(bookmarkRepository.count());
        metric.setComments(commentRepository.count());
        metric.setListenedStories(listenedStoryRepository.count());
        metric.setRecentStories(recentStoryRepository.count());
        metric.setRatedStories(ratingRepository.count());
        metric.setUserRatings(userRatingRepository.count());
        metric.setListenedTime(userRepository
            .findAll()
            .stream()
            .map(userMetricService::getListenedTime)
            .mapToLong(i -> i)
            .sum()
        );

        return applicationMetricRepository.save(metric);

    }
}
