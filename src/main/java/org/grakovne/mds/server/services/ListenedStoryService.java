package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.ListenedStory;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.repositories.ListenedStoryRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class ListenedStoryService {

    private final ListenedStoryRepository listenedStoryRepository;
    private final StoryService storyService;

    public ListenedStoryService(ListenedStoryRepository listenedStoryRepository, StoryService storyService) {
        this.listenedStoryRepository = listenedStoryRepository;
        this.storyService = storyService;
    }

    public ListenedStory createOne(Integer storyId, User user) {
        Story story = storyService.findOne(storyId);

        return listenedStoryRepository.findByUserAndStory(user, story).orElseGet(() -> {
            ListenedStory listenedStory = new ListenedStory();
            listenedStory.setCreateDateTime(LocalDateTime.now());
            listenedStory.setStory(story);
            listenedStory.setUser(user);

            return persist(listenedStory);
        });
    }

    public void deleteOne(Integer storyId, User user) {
        Story story = storyService.findOne(storyId);

        listenedStoryRepository.findByUserAndStory(user, story)
            .filter(listenedStory -> listenedStory.getUser().getId().equals(user.getId()))
            .ifPresent(listenedStoryRepository::delete);
    }

    public void deleteAll(User user) {
        listenedStoryRepository.deleteAll(listenedStoryRepository.findAllByUser(user));
    }

    private ListenedStory persist(ListenedStory listenedStory) {
        return listenedStoryRepository
            .findByUserAndStory(listenedStory.getUser(), listenedStory.getStory())
            .orElse(listenedStoryRepository.save(listenedStory));
    }
}
