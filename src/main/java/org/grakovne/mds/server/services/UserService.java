package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.UserRole;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.providers.ConfigurationProvider;
import org.grakovne.mds.server.repositories.UserRepository;
import org.grakovne.mds.server.repositories.UserRoleRepository;
import org.grakovne.mds.server.rest.forms.UserForm;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.google.common.hash.Hashing.sha512;
import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class UserService implements UserDetailsService {

    private static final String USER_ROLE_NAME = "USER";
    private static final String ADMIN_ROLE_NAME = "ADMIN";

    private final UserRepository userRepository;
    private final ConfigurationProvider configurationProvider;
    private final UserRoleRepository userRoleRepository;

    public UserService(UserRepository userRepository,
                       ConfigurationProvider configurationProvider,
                       UserRoleRepository userRoleRepository) {

        this.userRepository = userRepository;
        this.configurationProvider = configurationProvider;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public User loadUserByUsername(String username) {
        return userRepository
            .findByUsername(username)
            .orElseThrow(() -> new EntityNotFoundException(User.class));
    }

    public User findOne(Integer userId) {
        return userRepository
            .findOneById(userId)
            .orElseThrow(() -> new EntityNotFoundException(User.class));
    }

    public Page<User> findAll(Integer pageNumber) {
        return userRepository.findAll(PageRequest.of(pageNumber, configurationProvider.getPageSize()));
    }

    public User createOne(UserForm form) {
        return userRepository.findByUsername(form.getUsername())
            .orElseGet(() -> {
                User user = new User();
                user.setUsername(form.getUsername());
                user.setPassword(sha512().hashString(form.getPassword(), UTF_8).toString());
                user.setAccountNonExpired(true);
                user.setAccountNonLocked(true);
                user.setCredentialsNonExpired(true);
                user.setEnabled(true);
                user.setUserRoles(List.of(userRoleRepository.findByName(USER_ROLE_NAME).orElseThrow()));
                return userRepository.save(user);
            });
    }

    public boolean isAdmin(User user) {
        return user.getUserRoles()
            .contains(userRoleRepository
                .findByName(ADMIN_ROLE_NAME)
                .orElseThrow(() -> new EntityNotFoundException(UserRole.class)));
    }
}
