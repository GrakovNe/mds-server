package org.grakovne.mds.server.services;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.repositories.BookmarkRepository;
import org.grakovne.mds.server.rest.forms.BookmarkForm;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class BookmarkService {

    private final BookmarkRepository bookmarkRepository;
    private final StoryService storyService;
    private final UserService userService;

    public BookmarkService(BookmarkRepository bookmarkRepository,
                           StoryService storyService,
                           UserService userService) {

        this.bookmarkRepository = bookmarkRepository;
        this.storyService = storyService;
        this.userService = userService;
    }

    public List<Bookmark> findAllForUser(Integer storyId, Integer userId) {
        Story story = storyService.findOne(storyId);
        User user = userService.findOne(userId);

        return bookmarkRepository.findAllByUserAndStory(user, story);
    }

    public Optional<Bookmark> findLast(Integer storyId, User user) {
        Story story = storyService.findOne(storyId);

        return bookmarkRepository.findTopByUserAndStoryOrderByCreateDateTimeDesc(user, story);
    }

    public Bookmark createOne(Integer storyId, BookmarkForm form, User user) {
        Story story = storyService.findOne(storyId);

        return bookmarkRepository
            .findAllByUserAndStoryAndTimestamp(user, story, form.getTimestamp())
            .stream()
            .findAny()
            .orElseGet(() -> {
                Bookmark bookmark = new Bookmark();
                bookmark.setTimestamp(form.getTimestamp());
                bookmark.setUser(user);
                bookmark.setStory(story);
                bookmark.setCreateDateTime(LocalDateTime.now());
                return bookmarkRepository.save(bookmark);
            });
    }

    public Bookmark findOne(Integer bookmarkId, Integer userId) {
        User user = userService.findOne(userId);

        return bookmarkRepository.findOneById(bookmarkId)
            .filter(bookmark -> bookmark.getUser().getId().equals(user.getId()))
            .orElseThrow(() -> new EntityNotFoundException(Bookmark.class));
    }

    public void deleteOne(Integer bookmarkId, Integer userId) {
        Bookmark bookmark = findOne(bookmarkId, userId);
        bookmarkRepository.delete(bookmark);
    }

    public void deleteAll(User user) {
        bookmarkRepository.deleteAll(bookmarkRepository.findAllByUser(user));
    }

}
