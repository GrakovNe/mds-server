package org.grakovne.mds.server.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Rating extends MdsEntity {
    @OneToOne
    @JoinColumn(name = "story_id")
    @JsonIgnore
    private Story story;

    private Double value;

    private Integer voters;

    public Rating() {
    }

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Integer getVoters() {
        return voters;
    }

    public void setVoters(Integer voters) {
        this.voters = voters;
    }

    @Override
    public String toString() {
        return "Rating{" +
            "id=" + id +
            ", value=" + value +
            ", voters=" + voters +
            '}';
    }
}
