package org.grakovne.mds.server.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "comment")
public class Comment extends MdsEntity {
    @OneToOne
    @JoinColumn(name = "story_id")
    private Story story;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "user_name")
    private String username;

    private String message;

    @Column(name = "create_date_time")
    private LocalDateTime createdDateTime;

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(LocalDateTime createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "Comment{" +
            "id=" + id +
            ", story=" + story +
            ", user=" + user +
            ", username='" + username + '\'' +
            ", message='" + message + '\'' +
            ", createdDateTime=" + createdDateTime +
            '}';
    }
}
