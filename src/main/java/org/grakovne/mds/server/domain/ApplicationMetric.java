package org.grakovne.mds.server.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "application_metrics")
public class ApplicationMetric extends MdsEntity {
    private Long users;
    private Long stories;
    private Long authors;
    private Long bookmarks;
    private Long comments;
    private Long listenedStories;
    private Long recentStories;
    private Long ratedStories;
    private Long userRatings;
    private Long listenedTime;
    private LocalDateTime createDateTime;

    public Long getUsers() {
        return users;
    }

    public void setUsers(Long users) {
        this.users = users;
    }

    public Long getStories() {
        return stories;
    }

    public void setStories(Long stories) {
        this.stories = stories;
    }

    public Long getAuthors() {
        return authors;
    }

    public void setAuthors(Long authors) {
        this.authors = authors;
    }

    public Long getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(Long bookmarks) {
        this.bookmarks = bookmarks;
    }

    public Long getComments() {
        return comments;
    }

    public void setComments(Long comments) {
        this.comments = comments;
    }

    public Long getListenedStories() {
        return listenedStories;
    }

    public void setListenedStories(Long listenedStories) {
        this.listenedStories = listenedStories;
    }

    public Long getRecentStories() {
        return recentStories;
    }

    public void setRecentStories(Long recentStories) {
        this.recentStories = recentStories;
    }

    public Long getRatedStories() {
        return ratedStories;
    }

    public void setRatedStories(Long ratedStories) {
        this.ratedStories = ratedStories;
    }

    public Long getUserRatings() {
        return userRatings;
    }

    public void setUserRatings(Long userRatings) {
        this.userRatings = userRatings;
    }

    public Long getListenedTime() {
        return listenedTime;
    }

    public void setListenedTime(Long listenedTime) {
        this.listenedTime = listenedTime;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }
}
