package org.grakovne.mds.server.domain;

import javax.persistence.Entity;

@Entity
public class Author extends MdsEntity {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Author{" +
            "id=" + id +
            ", name='" + name + '\'' +
            '}';
    }

}
