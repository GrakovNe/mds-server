package org.grakovne.mds.server.domain.enums;

public enum State {
    ACTIVE,
    INACTIVE,
    REMOVED
}
