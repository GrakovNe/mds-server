package org.grakovne.mds.server.domain;

import org.grakovne.mds.server.domain.enums.State;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class StatefulEntity extends MdsEntity {

    @Enumerated(EnumType.STRING)
    protected State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
