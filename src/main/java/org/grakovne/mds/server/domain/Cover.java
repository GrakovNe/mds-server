package org.grakovne.mds.server.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Cover extends MdsEntity {
    @OneToOne
    @JoinColumn(name = "story_id")
    @JsonIgnore
    private Story story;

    @OneToOne
    @JoinColumn(name = "asset_id")
    private Asset asset;

    public Cover() {
    }

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }

    public Asset getAsset() {
        return asset;
    }

    public void setAsset(Asset asset) {
        this.asset = asset;
    }
}
