package org.grakovne.mds.server.providers;

import org.apache.commons.io.FileUtils;
import org.grakovne.mds.server.domain.Asset;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.grakovne.mds.server.exceptons.FileProviderException;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

@Component
public class FileProvider {

    private final ConfigurationProvider configurationProvider;

    public FileProvider(ConfigurationProvider configurationProvider) {
        this.configurationProvider = configurationProvider;
    }

    public File uploadFile(File file) {
        if (!Files.exists(getUploadFolder().toPath())) {
            createUploadDir();
        }

        String pathToSave = getUploadFolder() + File.separator + UUID.randomUUID().toString();
        File savedFile = new File(pathToSave);

        try {
            FileUtils.copyFile(file, savedFile);
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileProviderException("Can't save file.");
        }

        deleteFile(file);

        return savedFile;
    }


    public File getFile(String fileName) {
        String filePath = getUploadFolder() + File.separator + fileName;
        File file = new File(filePath);

        if (!file.exists()) {
            throw new EntityNotFoundException(Asset.class);
        }

        return file;
    }


    public void deleteFile(File file) {
        if (file.exists()) {
            file.delete();
        }
    }

    private File getUploadFolder() {
        return new File(configurationProvider.getFileUploadDirectory());
    }

    private void createUploadDir() {
        File uploadDir = new File(configurationProvider.getFileUploadDirectory());
        uploadDir.mkdirs();
    }
}

