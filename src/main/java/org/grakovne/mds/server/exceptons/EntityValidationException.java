package org.grakovne.mds.server.exceptons;

public class EntityValidationException extends MdsException {
    public EntityValidationException(Class mdsEntity, String validationIssue) {
        super(mdsEntity.getSimpleName() + " : " + validationIssue);
    }

}
