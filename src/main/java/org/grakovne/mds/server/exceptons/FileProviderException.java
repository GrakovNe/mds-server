package org.grakovne.mds.server.exceptons;

public class FileProviderException extends MdsException {

    public FileProviderException(String message) {
        super(message);
    }
}
