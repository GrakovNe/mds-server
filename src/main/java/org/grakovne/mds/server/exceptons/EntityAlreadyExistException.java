package org.grakovne.mds.server.exceptons;

import org.grakovne.mds.server.domain.MdsEntity;

public class EntityAlreadyExistException extends MdsException {
    public EntityAlreadyExistException(Class<? extends MdsEntity> mdsEntity) {
        super(mdsEntity.getSimpleName() + " already exists");
    }
}
