package org.grakovne.mds.server.exceptons;

import org.grakovne.mds.server.domain.MdsEntity;

public class EntityNotFoundException extends MdsException {

    public EntityNotFoundException(Class<? extends MdsEntity> mdsEntity) {
        super(mdsEntity.getSimpleName() + " not found");
    }
}
