package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Bookmark;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BookmarkRepository extends MdsRepository<Bookmark> {

    List<Bookmark> findAllByUserAndStory(User user, Story story);

    List<Bookmark> findAllByUserAndStoryAndTimestamp(User user, Story story, Integer timestamp);

    List<Bookmark> findAllByUser(User user);

    Optional<Bookmark> findTopByUserAndStoryOrderByCreateDateTimeDesc(User user, Story story);

    List<Bookmark> findByUserOrderByCreateDateTimeDesc(User user);
}
