package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.ListenedStory;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ListenedStoryRepository extends MdsRepository<ListenedStory> {

    Optional<ListenedStory> findByUserAndStory(User user, Story story);

    List<ListenedStory> findAllByUser(User user);

    long countAllByUser(User user);
}
