package org.grakovne.mds.server.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public interface MdsRepository<Entity> extends JpaRepository<Entity, Integer> {

    Optional<Entity> findOneById(Integer id);

}
