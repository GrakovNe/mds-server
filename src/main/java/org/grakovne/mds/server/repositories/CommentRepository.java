package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Comment;
import org.grakovne.mds.server.domain.Story;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CommentRepository extends MdsRepository<Comment> {

    Page<Comment> findAllByStoryOrderByCreatedDateTimeDesc(Story story, Pageable pageable);
}
