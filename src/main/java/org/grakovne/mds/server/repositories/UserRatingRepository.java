package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.UserRating;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRatingRepository extends MdsRepository<UserRating> {

    Optional<UserRating> findByUserAndStory(User user, Story story);

    long countAllByUser(User user);

    List<UserRating> findAllByUser(User user);
}
