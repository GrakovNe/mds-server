package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Author;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorRepository extends MdsRepository<Author> {

    Optional<Author> findByName(String name);

}
