package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.ApplicationMetric;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ApplicationMetricRepository extends MdsRepository<ApplicationMetric> {
    Page<ApplicationMetric> findByOrderByCreateDateTimeDesc(Pageable pageable);
}
