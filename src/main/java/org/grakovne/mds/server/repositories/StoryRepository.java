package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.exceptons.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StoryRepository extends MdsRepository<Story> {

    @Query("from Story s order by random()")
    Page<Story> findRandomStory(Pageable pageable);

    default Story findRandomStory() {
        return findRandomStory(PageRequest.of(0, 1))
            .getContent()
            .stream()
            .findAny()
            .orElseThrow(() -> new EntityNotFoundException(Story.class));
    }
}
