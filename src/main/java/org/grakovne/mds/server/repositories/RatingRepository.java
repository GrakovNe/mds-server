package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Rating;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends MdsRepository<Rating> {
}
