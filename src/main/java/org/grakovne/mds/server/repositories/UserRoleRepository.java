package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.UserRole;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRoleRepository extends MdsRepository<UserRole> {

    Optional<UserRole> findByName(String name);
}
