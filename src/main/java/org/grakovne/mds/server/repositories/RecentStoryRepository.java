package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.RecentStory;
import org.grakovne.mds.server.domain.Story;
import org.grakovne.mds.server.domain.User;
import org.grakovne.mds.server.domain.enums.State;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecentStoryRepository extends MdsRepository<RecentStory> {

    Optional<RecentStory> findByStoryAndUser(Story story, User user);

    List<RecentStory> findAllByUser(User user);

    Page<RecentStory> findAllByUserAndStateOrderByCreateDateTimeDesc(User user, State state, Pageable pageable);

    long countAllByUser(User user);
}
