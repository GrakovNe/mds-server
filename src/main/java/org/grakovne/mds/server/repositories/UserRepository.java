package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.User;

import java.util.Optional;

public interface UserRepository extends MdsRepository<User> {

    Optional<User> findByUsername(String username);
}
