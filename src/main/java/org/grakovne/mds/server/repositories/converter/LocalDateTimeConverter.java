package org.grakovne.mds.server.repositories.converter;

import org.apache.commons.lang.ArrayUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.parse;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Byte[]> {

    @Override
    public Byte[] convertToDatabaseColumn(final LocalDateTime localDateTime) {
        if (localDateTime == null) {
            return null;
        }

        try (final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
             final ObjectOutputStream outputStream = new ObjectOutputStream(buffer)) {
            outputStream.writeObject(localDateTime);
            return ArrayUtils.toObject(buffer.toByteArray());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public LocalDateTime convertToEntityAttribute(final Byte[] bytes) {
        if (bytes == null) {
            return null;
        }

        try (final ByteArrayInputStream inputStream = new ByteArrayInputStream(ArrayUtils.toPrimitive(bytes));
             final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream)) {

            return parse(objectInputStream.readObject().toString());
        } catch (Exception e) {
            return null;
        }

    }
}
