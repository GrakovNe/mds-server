package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Cover;
import org.springframework.stereotype.Repository;

@Repository
public interface CoverRepository extends MdsRepository<Cover> {
}
