package org.grakovne.mds.server.repositories;

import org.grakovne.mds.server.domain.Asset;

import java.util.Optional;

public interface AssetRepository extends MdsRepository<Asset> {

    Optional<Asset> findByHash(String hash);

    Optional<Asset> findById(Long id);
}
